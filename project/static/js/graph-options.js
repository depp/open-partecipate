if(!window.graph_presets) window.graph_presets = {};

graph_presets['controllanti-controllate'] = {
    "height": 800,
    "node_label": "Partecipata",
    "node_id": "0",
    "node_is_head": function (label) {
        return (label === 'Controllata');
    },
    "node_radius": 30,
    "classes2nodes": {
        "Partecipata": ["#04003B", "partecipata"],
        "Controllante - Partecipata": ["#0066CC", "controllante-partecipata"],
        "Controllante - Ente pubblico": ["#E4B73F", "controllante-ente-pubblico"],
        "Controllante - Ente privato": ["#62C8BB", "controllante-ente-privato"],
        "Controllante - Persona fisica": ["#C8627C", "controllante-persona-fisica"],
        "Controllata": ["#ACCEF2", "controllata"],
    }
};
