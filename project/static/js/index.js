var Openpartecipate_pwa_01 = (function () {
    "use strict";

    // INTERNAL PROPERTIES
    var _lastPopup = null;
    var _hoveredStateId = null;

    var _init = function () {
        Highcharts.setOptions({
            lang: {
                numericSymbols: [null, ' mln', ' mld'],
                decimalPoint: ',',
                thousandsSep: '.'
            }
        });

        $('button').tooltip();

        const prefix = '!';
        const hash = window.location.hash.replace('#' + prefix, '#');
        $('#tipologie').find('.nav-link').each(function () {
            if (hash === $(this).attr('href')) {
                $(this).trigger('click');
            }
            $(this).on('click', function () {
                window.location.hash = $(this).attr('href').replace('#', '#' + prefix);
            })
        });

        $('select#anno-rilevazione,select#order-by').on('change', function () {
            window.location.href = $(this).val();
        });

        $('input.js-typeahead').each(function () {
            $(this).typeahead($.extend(true, {}, window.typeahead_presets));
        }).prop('disabled', false);

        _makeCharts();
        _makeMap();
        _makeGraph();
    };

    var _generateId = function (length) {
        var result = '_';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    };

    var _makeCharts = function () {
        $('.table2chart').each(function () {
            // get the relevant options
            const CO = $(this).data('chart-options');
            const options = $.extend(true, {}, window.highcharts_presets['default'], window.highcharts_presets[CO]);

            const max = $(this).data('chart-max');

            // generate an id and apply to the element
            const table_id = _generateId(8);
            $(this).attr('id', table_id).prop('id', table_id).hide();

            if (CO === 'tree') {
                const svgpath = $(this).data('chart-svgpath');
                var data = [];
                $(this).find('tbody tr').each(function () {
                    const label = $(this).find('td').eq(0).text();
                    const href = $(this).find('td').eq(0).find('a').attr('href');
                    const pattern = $(this).find('td').eq(1).text().split(' ').map(function (x) {
                        return x.substring(0, 3).toLowerCase()
                    }).join('_');
                    const value = Number($(this).find('td').eq(2).text());
                    data.push([label, href, value, svgpath + pattern + '.svg']);
                });
                options.series = [{
                    'keys': ['name', 'href', 'value', 'color.pattern.image'],
                    'data': data
                }]
            } else {
                options.data = { table: table_id };
            }

            if (max) {
                options.yAxis.max = max;
            }

            $('<div id="' + table_id + '_wrapper" class="highcharts-wrapper ' + CO + '_wrapper"></div>')
                .insertBefore(this)
                .css('height', options.chart.height);

            Highcharts.chart(table_id + '_wrapper', options);
        })
    };

    var _makeMap = function () {
        const mapHighlightColor = '#0B1452';

        $('.table2map').each(function () {
            // get the relevant options
            const MO = $(this).data('map-options');
            const options = $.extend(true, {}, window.map_presets[MO], $(this).data('map-extra-options'));

            const highlight = $(this).data('map-highlight');

            // generate an id and apply to the element
            const table_id = _generateId(8);
            $(this).attr('id', table_id).prop('id', table_id).hide();

            // generate a map wrapper and attach it to the DOM
            $('<div id="' + table_id + '_wrapper" class="map-wrapper ' + table_id + '_wrapper"></div>')
                .insertBefore(this)
                .css('width', '100%')
                .css('height', options.height);

            // retrive values from html, find max and index by id
            var data = [];
            var maxVal = 0;
            $(this).find('tbody tr').each(function () {
                const label = $(this).find('td').eq(0).text();
                const href = $(this).find('td').eq(0).find('a').attr('href');
                const id = $(this).find('td').eq(1).text();
                const value = Number($(this).find('td').eq(2).text());
                if (value > maxVal) {
                    maxVal = value;
                }
                data.push({label: label, href: href, id: id, value: value});
            });

            var dataById = data.reduce(function (result, currentObject) {
                result[currentObject.id] = currentObject;
                return result;
            }, {});

            // calculate choropleth
            const expression = ['match', ['get', 'COD_REG']];
            const palette = ['#B3CBDC', '#125CA4'];
            const colorScale = chroma.scale(palette);

            if (!highlight) {
                for (let i = 0; i < data.length; i++) {
                    const rangeVal = (data[i].value / maxVal);
                    const color = colorScale(rangeVal).hex();
                    expression.push(data[i].id, color);
                }
            } else {
                expression.push(highlight, mapHighlightColor);
            }

            expression.push('#DADADA');

            // initialize the map
            var map = new mapboxgl.Map({
                container: table_id + '_wrapper',
                style: options,
                center: [13, 42], // starting position [lng, lat]
                zoom: 3.9 // starting zoom
            });
            map.scrollZoom.disable();

            // apply choropleth to the map
            map.on('load', function () {
                const newprop = [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    mapHighlightColor,
                    expression
                ];
                map.setPaintProperty('regioni', 'fill-color', newprop);
            });

            // initialize event handlers
            _bindEvents(map, dataById);
        });
    };

    var _bindEvents = function (map, dataById) {
        map.on('mouseenter', 'regioni', function (e) {
            _show(e, map, dataById)
        });
        map.on('mouseleave', 'regioni', function (e) {
            _hide(e, map, dataById)
        });
        map.on('mousemove', 'regioni', function (e) {
            _move(e, map, dataById)
        });
        map.on('click', 'regioni', function (e) {
            _click(e, map, dataById)
        });
    };

    var _show = function (e, map, dataById) {
        var html = _getPopup(e.features[0], dataById);

        _lastPopup = new mapboxgl.Popup({closeButton: false, offset: 20, maxWidth: '600px', closeOnClick: false})
            .setLngLat(e.lngLat)
            .setHTML(html)
            .addTo(map);
    };

    var _hide = function (e, map, dataById) {
        map.getCanvas().style.cursor = '';

        if (_hoveredStateId) {
            map.setFeatureState({source: 'regioni', id: _hoveredStateId}, {hover: false});
            _hoveredStateId = null;
        }

        if (_lastPopup) {
            _lastPopup.remove();
            _lastPopup = null;
        }
    };

    var _move = function (e, map, dataById) {
        if (e.features.length > 0) {
            map.getCanvas().style.cursor = 'pointer';

            if (_hoveredStateId) {
                map.setFeatureState({source: 'regioni', id: _hoveredStateId}, {hover: false});
            }

            _hoveredStateId = e.features[0].id;

            map.setFeatureState({source: 'regioni', id: _hoveredStateId}, {hover: true});

            var html = _getPopup(e.features[0], dataById);
            _lastPopup.setHTML(html).setLngLat(e.lngLat)
        }
    };

    var _click = function (e, map, dataById) {
        var id = e.features[0].properties.COD_REG;
        var href = dataById[id].href;
        if (href) {
            window.location.href = href;
        }
    };

    var _getPopup = function (feature, dataById) {
        var id = feature.properties.COD_REG;

        var html = '<div class="map-popup">';
        html += '<div class="title">' + dataById[id].label + '</div>';
        html += 'Totale delle partecipate';
        html += '<h2>' + dataById[id].value + '</h2>';
        html += '</div>';
        return html
    };

    var _makeGraph = function () {
        $('.table2graph').each(function () {
            // get the relevant options
            const GO = $(this).data('graph-options');
            const options = $.extend(true, {}, window.graph_presets[GO]);

            const svgpath = $(this).data('graph-svgpath');
            const node_name = $(this).data('graph-nodename');

            const node_label = options.node_label;
            const node_id = options.node_id;

            const classes2nodes = options.classes2nodes;

            // generate an id and apply to the element
            const table_id = _generateId(8);
            $(this).attr('id', table_id).prop('id', table_id).hide();

            var nodes = [];
            var relationships = [];
            var colors = [];

            nodes.push({
                'id': node_id,
                'labels': [node_label],
                'properties': {
                    'denominazione': node_name
                }
            });
            colors.push(classes2nodes[node_label][0]);

            $(this).find('tbody tr').each(function () {
                const id = nodes.length.toString();
                const label = $(this).find('td').eq(1).text();

                nodes.push({
                    'id': id,
                    'labels': [label],
                    'properties': {
                        'denominazione': $(this).find('td').eq(0).text(),
                        'quota': $(this).find('td').eq(2).text(),
                        'href': $(this).find('td').eq(0).find('a').attr('href')
                    }
                });

                relationships.push({
                    'id': id,
                    'type': $(this).find('td').eq(2).text(),
                    'startNode': options.node_is_head(label) ? node_id : id,
                    'endNode': options.node_is_head(label) ? id : node_id,
                    'properties': {}
                });

                if (colors.indexOf(classes2nodes[label][0]) === -1) {
                    colors.push(classes2nodes[label][0])
                }
            });

            var images = {};
            $.each(classes2nodes, function (k, v) {
                images[k] = svgpath + v[1] + '.svg';
            });

            var container = $('<div id="' + table_id + '_wrapper"></div>')
                .insertBefore(this)
                .css('height', options.height);

            container.before($('<div></div>').addClass('neo4jd3-legend').append(
                $.map(classes2nodes, function (value, key) {
                    if (colors.indexOf(value[0]) !== -1) {
                        return [$('<span></span>').addClass('rect').css('background-color', value[0]), $('<span></span>').addClass('fs-13').text(key)];
                    }
                })
            ));

            new Neo4jd3('#' + table_id + '_wrapper', {
                highlight: [{
                    class: node_label
                }],
                images: images,
                colors: colors,
                nodeRadius: options.node_radius,
                neo4jData: {
                    'results': [{
                        'data': [{
                            'graph': {
                                'nodes': nodes,
                                'relationships': relationships
                            }
                        }]
                    }],
                    'errors': []
                },
                onNodeClick: function(node) {
                    var href = node.properties.href;
                    if (href) {
                        window.location.href = href;
                    }
                }
            });

            container.find('title').remove();
        });
    };

    return {
        init: _init
    };
})();

if (document.readyState === 'complete') {
    Openpartecipate_pwa_01.init();
} else {
    window.addEventListener('load', Openpartecipate_pwa_01.init, 0);
}
