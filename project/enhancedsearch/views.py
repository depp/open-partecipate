# -*- coding: utf-8 -*-
from haystack.generic_views import FacetedSearchView
from .forms import MultiSelectWithRangeFacetedSearchForm, format_facet_field


class MultiSelectWithRangeFacetedSearchView(FacetedSearchView):
    load_all = False
    form_class = MultiSelectWithRangeFacetedSearchForm
    model = None
    facet_fields = ()
    FACETS = ()
    RANGES = {}

    def get_queryset(self):
        queryset = super().get_queryset()

        if self.model:
            queryset = queryset.models(self.model)

        for name in self.FACETS:
            queryset = queryset.facet(format_facet_field(name))

        for name in self.RANGES:
            for range in self.RANGES[name]:
                queryset = queryset.query_facet(format_facet_field(name), self.RANGES[name][range]['qrange'])

        return queryset

    @staticmethod
    def _get_objects_by_pk(pks):
        return {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        objects_by_pk = self._get_objects_by_pk(object.pk for object in context['object_list'])
        for object in context['object_list']:
            try:
                object.object = objects_by_pk[object.pk]
            except KeyError:
                pass

        return context

    def _build_facet_field_info(self, field, label, key_to_labels):
        facet_counts_fields = self.queryset.facet_counts().get('fields', {})

        facet = {}
        if field in facet_counts_fields:
            facet['label'] = label
            facet['values'] = [
                {
                    'key': c[0],
                    'label': key_to_labels.get(c[0], c[0]),
                    'count': c[1],
                    'urls': self._get_facet_urls(field, c[0]),
                } for c in facet_counts_fields[field]]

        return facet

    def _build_range_facet_queries_info(self, field, label):
        facet_counts_queries = self.queryset.facet_counts().get('queries', {})

        facet = {}
        if field in self.RANGES:
            ranges = self.RANGES[field]

            facet['label'] = label
            facet['values'] = [
                {
                    'key': ranges[range]['qrange'],
                    'label': ranges[range]['label'],
                    'count': facet_counts_queries.get('{}:{}'.format(format_facet_field(field), ranges[range]['qrange'])),
                    'urls': self._get_facet_urls(field, ranges[range]['qrange']),
                } for range in sorted(ranges.keys())]

        return facet

    def _get_facet_urls(self, facet, key):
        facet_key = '{}:{}'.format(facet, key)

        params = self.params

        urls = {'add_filter': False, 'remove_filter': False}

        selected_facets = params.getlist('selected_facets')
        if facet_key in selected_facets:
            selected_facets.remove(facet_key)
            k = 'remove_filter'
        else:
            selected_facets.append(facet_key)
            k = 'add_filter'
        params.setlist('selected_facets', selected_facets)
        urls[k] = params.urlencode(safe=':')

        return urls

    @property
    def params(self):
        params = self.request.GET.copy()

        if 'q' not in params:
            params['q'] = ''
        if 'page' in params:
            del(params['page'])

        params.setlist('selected_facets', sorted(set(params.getlist('selected_facets'))))

        return params
