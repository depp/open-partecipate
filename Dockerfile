FROM python:3.8-slim

# Update, upgrade and install useful tools and set aliases
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -qy update \
    && apt-get install -qqy apt-utils \
    && apt-get -qqy upgrade \
    && apt-get install -y --no-install-recommends binutils libproj-dev gdal-bin \
    && apt-get install -qqqy --no-install-recommends \
        gcc \
        git \
        locales \
        python3-dev \
        libxml2-dev libxslt-dev \
        tmux \
        jq \
        less \
        vim \
    && rm -rf /var/lib/apt/lists/*
RUN echo 'alias ll="ls -l"' >> ~/.bashrc \
    && echo 'alias la="ls -la"' >> ~/.bashrc

# add it locale
COPY locales.txt /etc/locale.gen
RUN locale-gen

# initialize app
RUN mkdir -p /app
WORKDIR /app

# upgrade pip
# Install projects requirements
COPY requirements.txt /app
RUN pip3 install -r requirements.txt

# remove gcc and builkd dependencies to keep image small
RUN apt-get purge -y --auto-remove gcc python3-dev

# copy app
COPY . /app/

RUN rm /app/locales.txt
