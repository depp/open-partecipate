from .models import Regione, Settore, Categoria, Tooltip


def main_settings(request):
    host = request.META['HTTP_HOST']

    return {
        'IS_PRODUCTION': all(h not in host for h in ('localhost', 'staging')),
        'REGIONI': Regione.objects.all(),
        'SETTORI': Settore.objects.all(),
        'CATEGORIE': Categoria.objects.filter(gruppo='categoria'),
        'TIPOLOGIE_CONTROLLANTI': Categoria.objects.filter(gruppo='tipologia_entecontrollante'),
        'TOOLTIPS': dict(Tooltip.objects.values_list('identificativo', 'testo')),
    }
