from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'project.core.api'
    verbose_name = 'Open Partecipate - API'
