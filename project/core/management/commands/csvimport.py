# -*- coding: utf-8 -*-
import datetime
import logging
import re

import pandas as pd
import urllib.request
import zipfile
from django.db import transaction, connection, DatabaseError
from django.core.management.base import BaseCommand
from io import BytesIO

from ...models import *


def convert_int(text):
    try:
        return int(convert_float(text))
    except (TypeError, ValueError):
        return None


def convert_float(text):
    try:
        return float(text.replace('.', '').replace(',', '.'))
    except ValueError:
        return None


def convert_provincia(text):
    text = text.strip("' ")

    if len(text) == 4 and text[0] == '(' and text[-1] == ')':
        text = text.strip('()')

    if len(text) == 2:
        text = text.upper()
    else:
        text = text.replace(' - ', '-')

    return text


def convert_quotata(text):
    if text:
        return text.upper() != 'NO'
    else:
        return None


def convert_dipendenti(text):
    text = text.split('\n')[0]
    if text == 'zero':
        text = '0'
    return convert_float(text)


def convert_nomecontrollante(text):
    return re.sub('\s{2,}', ' ', text).strip().upper()\
        .replace('COMUNEDI ', 'COMUNE ').replace('COMUNED ', 'COMUNE ').replace('CCOMUNE ', 'COMUNE ').replace('COMUNE DI', 'COMUNE ').replace('  ', ' ').replace('COMUNE ', 'COMUNE DI ')\
        .replace("A'", 'À').replace("E'", 'È').replace("I'", 'Ì').replace("O'", 'Ò').replace("U'", 'Ù')\
        .replace(' CÀ ', " CA' ").replace(' DÈ ', " DE' ").replace("D' ", "D'").replace("T' ", "T'")\
        .replace('S.P.A.', 'SPA').replace('S.P.A', 'SPA')


def convert_codicefiscale(text):
    return text.strip("'").strip()


class Command(BaseCommand):
    """
    Data are imported from their CSV sources.
    """
    help = 'Import data from csv'

    def add_arguments(self, parser):
        parser.add_argument('archive_file', nargs=1, help='Path of the archive file')

    logger = logging.getLogger(__name__)

    def read_csv(self, csv):
        df = pd.read_csv(
            BytesIO(csv),
            sep=';',
            header=0,
            low_memory=True,
            dtype=object,
            encoding='latin1',
            keep_default_na=False,
            converters={
                'provincia': convert_provincia,
                'quotata': convert_quotata,
                'dipendenti': convert_dipendenti,
                'TotEntrate': convert_int,
                'Spese': convert_int,
                'SPersonale': convert_int,
                'SInvestimenti': convert_int,
                'numeroUL': convert_int,
                'Progressivo': convert_int,
                'progr': convert_int,
                'nomeControllante': convert_nomecontrollante,
                'codiceFiscaleControllante': convert_codicefiscale,
                'codiceFiscaleControllato': convert_codicefiscale,
                'percControllo': convert_float,
                'SettorePerc': convert_float,
                'quota': convert_float,
            }
        )
        df = df.where((pd.notnull(df)), None)

        return df

    def handle(self, *args, **options):
        archivefile = options['archive_file'][0]

        self.stdout.write('Lettura file {} ....'.format(archivefile))

        if archivefile.startswith('http'):
            archive = urllib.request.urlopen(archivefile).read()
        else:
            import codecs
            with codecs.open(archivefile, 'r', 'latin1') as file:
                archive = bytes(file.read(), 'latin1')

        self.stdout.write('Inizio import.')

        start_time = datetime.datetime.now()

        self._truncate_table(Categoria)

        buffer = BytesIO(archive)

        with zipfile.ZipFile(buffer) as zfile:
            df_p = self.read_csv(zfile.read('X_ALL_OP_PARTECIPATE.csv'))
            df_u = self.read_csv(zfile.read('X_ALL_OP_UNITALOCALI.csv'))

        df = pd.merge(df_p, df_u, on=('id', 'AnnoRilevazione'), how='left')

        self.import_enti_partecipati(df)

        with zipfile.ZipFile(buffer) as zfile:
            df = self.read_csv(zfile.read('X_ALL_OP_CONTROLLANTI.csv'))

        self.import_enti_controllanti(df)

        with zipfile.ZipFile(buffer) as zfile:
            df = self.read_csv(zfile.read('X_ALL_OP_CONTROLLATI.csv'))

        self.import_enti_controllati(df)

        with zipfile.ZipFile(buffer) as zfile:
            df = self.read_csv(zfile.read('X_ALL_OP_MULTISETTORIALI.csv'))

        df.rename(columns={'annoRilevazione': 'AnnoRilevazione'}, inplace=True)

        self.import_settori(df)

        with zipfile.ZipFile(buffer) as zfile:
            df_p = self.read_csv(zfile.read('X_ALL_OP_PARTECIPATE.csv'))
            df_r = self.read_csv(zfile.read('X_ALL_OP_MULTIREGIONALI.csv'))

        # df_p = df_p[df_p['PNC'] != 'C']

        df_r.rename(columns={'annoRilevazione': 'AnnoRilevazione'}, inplace=True)
        df_r['id'] = df_r['id'].map(lambda x: x.replace(',00', ''))

        df = pd.merge(df_p, df_r, on=('id', 'AnnoRilevazione'), how='left')

        self.import_regioni(df)

        duration = datetime.datetime.now() - start_time
        seconds = round(duration.total_seconds())

        self.stdout.write('Fatto. Tempo di esecuzione: {:02d}:{:02d}:{:02d}.'.format(int(seconds // 3600), int((seconds % 3600) // 60), int(seconds % 60)))

    @transaction.atomic
    def import_enti_partecipati(self, df):
        self._truncate_table(EntePartecipato)
        self._truncate_table(EntePartecipatoCronologia)

        cache = {x[0]: {} for x in Categoria.GRUPPO_CHOICES}
        for obj in Categoria.objects.all():
            cache[obj.gruppo][obj.denominazione] = obj

        df = df.sort_values(['id', 'AnnoRilevazione'], ascending=[True, True]).rename(columns={'PA/EXTRA PA': 'tipologia', 'ambitoTerritoriale': 'governance'})

        df_count = len(df)

        ente_partecipato = EntePartecipato()

        for n, (index, row) in enumerate(df.iterrows(), 1):
            if ente_partecipato.id != row['id']:
                if row['PNC'] == 'C':
                    continue

                categorie = {}
                for fld, _ in [x for x in Categoria.GRUPPO_CHOICES if x[0] != 'tipologia_entecontrollante']:
                    if row[fld]:
                        if row[fld] not in cache[fld]:
                            cache[fld][row[fld]] = Categoria.objects.create(gruppo=fld, denominazione=row[fld])
                        categorie[fld] = cache[fld][row[fld]]

                try:
                    with transaction.atomic():
                        ente_partecipato = EntePartecipato.objects.create(
                            id=row['id'],
                            denominazione=row['partecipata'],
                            codice_fiscale=row['codiceFiscale'],
                            provincia=row['provincia'],
                            comune=row['comune'],
                            indirizzo=row['indirizzo'],
                            anno_inizio_attivita=row["annoInizioAttivita'"],
                            **categorie,
                        )
                except DatabaseError as e:
                    self.stdout.write(self.style.ERROR('{}/{} - ERRORE database: {} [{}]'.format(n, df_count, dict(row), e)))
                    continue
                else:
                    self.stdout.write('{}/{} - Creato ente partecipato: {}'.format(n, df_count, ente_partecipato))

            try:
                with transaction.atomic():
                    EntePartecipatoCronologia.objects.create(
                        ente_partecipato=ente_partecipato,
                        anno_rilevazione=row['AnnoRilevazione'],
                        stato=row['PNC'],
                        quotato=row['quotata'],
                        dipendenti=row['dipendenti'],
                        entrate_totali=row['TotEntrate'],
                        spese_totali=row['Spese'],
                        spese_personale=row['SPersonale'],
                        spese_investimenti=row['SInvestimenti'],
                        numero_unita_locali=row['numeroUL'] if pd.notnull(row['numeroUL']) else None,
                        livello_partecipazione={'PRIMO': '1', 'SECONDO': '2'}.get(row['LivelloPartecipa']),
                    )
            except DatabaseError as e:
                self.stdout.write(self.style.ERROR('{}/{} - ERRORE database: {} [{}]'.format(n, df_count, dict(row), e)))
            else:
                self.stdout.write('{}/{} - Creata cronologia per anno: {}'.format(n, df_count, row['AnnoRilevazione']))

    @transaction.atomic
    def import_enti_controllanti(self, df):
        self._truncate_table(EnteControllanteQuota)
        self._truncate_table(EnteControllante)

        cache = {
            **self._get_ente_partecipato_cronologia_cache(),
            'ente_controllante': {(o.denominazione, o.codice_fiscale, o.tipo): o for o in EnteControllante.objects.all()},
            'ente_controllante_tipologia': {o.denominazione: o for o in Categoria.objects.filter(gruppo='tipologia_entecontrollante')},
        }

        df = df[(df['nomeControllante'] != '') | (df['codiceFiscaleControllante'] != '') | (df['tipoenteControllante'] != '')]

        mappa = {}
        for by, df1 in df[['nomeControllante', 'codiceFiscaleControllante', 'tipoenteControllante']].drop_duplicates().groupby(['codiceFiscaleControllante', 'tipoenteControllante'], as_index=False):
            if len(df1) > 1:
                lst = list(df1['nomeControllante'])
                for itm in lst:
                    if 'COMUNE DI {}'.format(itm) in lst:
                        mappa[(itm,) + by] = ('COMUNE DI {}'.format(itm),) + by

        df_count = len(df)

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                ente_partecipato_cronologia = cache['ente_partecipato_cronologia'][(row['id'], row['AnnoRilevazione'])]
            except KeyError:
                self.stdout.write(self.style.ERROR('{}/{} - ERRORE cronologia mancante: {}'.format(n, df_count, dict(row))))
            else:
                key = (row['nomeControllante'], row['codiceFiscaleControllante'], row['tipoenteControllante'])
                key = mappa.get(key, key)
                if key not in cache['ente_controllante']:
                    if row['tipoenteControllante'] not in cache['ente_controllante_tipologia']:
                        cache['ente_controllante_tipologia'][row['tipoenteControllante']] = Categoria.objects.create(gruppo='tipologia_entecontrollante', denominazione=row['tipoenteControllante'])
                        self.stdout.write('{}/{} - Creata tipologia ente controllante: {}'.format(n, df_count, cache['ente_controllante_tipologia'][row['tipoenteControllante']]))
                    cache['ente_controllante'][key] = EnteControllante.objects.create(denominazione=row['nomeControllante'], codice_fiscale=row['codiceFiscaleControllante'], tipologia=cache['ente_controllante_tipologia'][row['tipoenteControllante']])
                    self.stdout.write('{}/{} - Creato ente controllante: {}'.format(n, df_count, cache['ente_controllante'][key]))

                try:
                    with transaction.atomic():
                        quota = EnteControllanteQuota.objects.create(
                            ente_partecipato_cronologia=ente_partecipato_cronologia,
                            ente_controllante=cache['ente_controllante'][key],
                            progressivo=row['Progressivo'],
                            quota=row['percControllo'],
                        )
                except DatabaseError as e:
                    self.stdout.write(self.style.ERROR('{}/{} - ERRORE database: {} [{}]'.format(n, df_count, dict(row), e)))
                else:
                    self.stdout.write('{}/{} - Creata quota ente controllante: {}'.format(n, df_count, quota))

    @transaction.atomic
    def import_enti_controllati(self, df):
        self._truncate_table(EnteControllatoQuota)
        self._truncate_table(EnteControllato)

        cache = {
            **self._get_ente_partecipato_cronologia_cache(),
            'ente_controllato': {(o.denominazione, o.codice_fiscale): o for o in EnteControllato.objects.all()},
        }

        df = df[(df['nomeControllato'] != '') | (df['codiceFiscaleControllato'] != '')]

        df_count = len(df)

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                ente_partecipato_cronologia = cache['ente_partecipato_cronologia'][(row['id'], row['AnnoRilevazione'])]
            except KeyError:
                self.stdout.write(self.style.ERROR('{}/{} - ERRORE cronologia mancante: {}'.format(n, df_count, dict(row))))
            else:
                key = (row['nomeControllato'], row['codiceFiscaleControllato'])
                if key not in cache['ente_controllato']:
                    cache['ente_controllato'][key] = EnteControllato.objects.create(denominazione=row['nomeControllato'], codice_fiscale=row['codiceFiscaleControllato'])
                    self.stdout.write('{}/{} - Creato ente controllato: {}'.format(n, df_count, cache['ente_controllato'][key]))

                try:
                    with transaction.atomic():
                        quota = EnteControllatoQuota.objects.create(
                            ente_partecipato_cronologia=ente_partecipato_cronologia,
                            ente_controllato=cache['ente_controllato'][key],
                            progressivo=row['progr'],
                            quota=row['percControllo'],
                        )
                except DatabaseError as e:
                    self.stdout.write(self.style.ERROR('{}/{} - ERRORE database: {} [{}]'.format(n, df_count, dict(row), e)))
                else:
                    self.stdout.write('{}/{} - Creata quota ente controllato: {}'.format(n, df_count, quota))

    @transaction.atomic
    def import_settori(self, df):
        self._truncate_table(SettoreQuota)
        self._truncate_table(Settore)

        cache = {
            **self._get_ente_partecipato_cronologia_cache(),
            'settore': Settore.objects.in_bulk(field_name='codice'),
        }

        df = df[df['settore'] != '']

        df_count = len(df)

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                ente_partecipato_cronologia = cache['ente_partecipato_cronologia'][(row['id'], row['AnnoRilevazione'])]
            except KeyError:
                self.stdout.write(self.style.ERROR('{}/{} - ERRORE cronologia mancante: {}'.format(n, df_count, dict(row))))
            else:
                codice, denominazione = [x.strip() for x in row['settore'].split(' - ', 1)]
                if codice not in cache['settore']:
                    cache['settore'][codice] = Settore.objects.create(codice=codice, denominazione=denominazione)
                    self.stdout.write('{}/{} - Creato settore: {}'.format(n, df_count, cache['settore'][codice]))

                try:
                    with transaction.atomic():
                        quota = SettoreQuota.objects.create(
                            ente_partecipato_cronologia=ente_partecipato_cronologia,
                            settore=cache['settore'][codice],
                            quota=row['SettorePerc'],
                        )
                except DatabaseError as e:
                    self.stdout.write(self.style.ERROR('{}/{} - ERRORE database: {} [{}]'.format(n, df_count, dict(row), e)))
                else:
                    self.stdout.write('{}/{} - Creata quota settore: {}'.format(n, df_count, quota))

    @transaction.atomic
    def import_regioni(self, df):
        self._truncate_table(RegioneQuota)
        # self._truncate_table(Regione)

        cache = {
            **self._get_ente_partecipato_cronologia_cache(),
            'regione':  Regione.objects.in_bulk(field_name='denominazione'),
        }

        df_count = len(df)

        for n, (index, row) in enumerate(df.iterrows(), 1):
            try:
                ente_partecipato_cronologia = cache['ente_partecipato_cronologia'][(row['id'], row['AnnoRilevazione'])]
            except KeyError:
                self.stdout.write(self.style.ERROR('{}/{} - ERRORE cronologia mancante: {}'.format(n, df_count, dict(row))))
            else:
                if pd.notnull(row['regione_y']):
                    denominazione = row['regione_y']
                    perc = row['quota']
                else:
                    denominazione = row['regione_x']
                    perc = 100

                # if denominazione not in cache['regione']:
                #     cache['regione'][denominazione] = Regione.objects.create(denominazione=denominazione)
                #     self.stdout.write('{}/{} - Creata regione: {}'.format(n, df_count, cache['regione'][denominazione]))

                try:
                    with transaction.atomic():
                        quota = RegioneQuota.objects.create(
                            ente_partecipato_cronologia=ente_partecipato_cronologia,
                            regione=cache['regione'][denominazione],
                            quota=perc,
                        )
                except KeyError:
                    self.stdout.write(self.style.ERROR('{}/{} - ERRORE regione mancante: {}'.format(n, df_count, dict(row))))
                except DatabaseError as e:
                    self.stdout.write(self.style.ERROR('{}/{} - ERRORE database: {} [{}]'.format(n, df_count, dict(row), e)))
                else:
                    self.stdout.write('{}/{} - Creata quota regione: {}'.format(n, df_count, quota))

    def _get_ente_partecipato_cronologia_cache(self):
        return {'ente_partecipato_cronologia':  {(str(o.ente_partecipato_id), o.anno_rilevazione): o for o in EntePartecipatoCronologia.objects.all()}}

    def _truncate_table(self, model):
        table = model._meta.db_table

        self.stdout.write('Truncate table "{}"'.format(table), ending='... ')

        try:
            with connection.cursor() as cursor:
                cursor.execute('TRUNCATE TABLE "{}" RESTART IDENTITY CASCADE;'.format(table))
        except Exception as e:
            self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
            exit(1)
        else:
            self.stdout.write(self.style.SUCCESS('OK'))
