if(!window.map_presets) window.map_presets = {};

map_presets['distribuzione-regionale'] = {
    "version": 8,
    "name": "Distribuzione regionale",
    "interactive": false,
    "height": 300,
    "center": [
        13,
        42
    ],
    "maxBounds": [2, 36.55, 23, 48],
    "zoom": 4,
    "sources": {
        "regioni": {
            "type": "geojson",
            "maxzoom": 11
        }
    },
    "layers": [
        {
            "id": "background",
            "type": "background",
            "paint": {
                "background-color": "#eeeeee"
            }
        },
        {
            "id": "regioni",
            "type": "fill",
            "source": "regioni",
            "minzoom": 3,
            "paint": {
                "fill-color": "#ffffff"
            }
        },
        {
            "id": "regioni-line",
            "type": "line",
            "source": "regioni",
            "minzoom": 3,
            "paint": {
                "line-width": 0,
                "line-color": "#000000"
            }
        }
    ],
    "id": "mapparegioni"
};
