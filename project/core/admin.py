from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.forms import FlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.forms import MultipleHiddenInput

from .models import Tooltip


@admin.register(Tooltip)
class TooltipAdmin(admin.ModelAdmin):
    list_display = ('identificativo', 'testo')

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if not request.user.is_superuser and obj:
            readonly_fields += ('identificativo',)
        return readonly_fields


class CustomFlatpageAdminForm(FlatpageForm):
    class Meta:
        widgets = {
            'content': CKEditorUploadingWidget(),
            'sites': MultipleHiddenInput()
        }


class CustomFlatPageAdmin(FlatPageAdmin):
    form = CustomFlatpageAdminForm
    list_filter = []
    fieldsets = (
        (None, {'fields': ('url', 'title', 'content', 'sites')}),
    )

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if not request.user.is_superuser and obj:
            readonly_fields += ('url',)
        return readonly_fields

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'sites':
            kwargs['initial'] = [Site.objects.get_current()]
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.unregister(FlatPage)
admin.site.register(FlatPage, CustomFlatPageAdmin)
