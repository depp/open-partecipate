from django.contrib.postgres.aggregates import StringAgg
from django.db.models import Count, Sum, F, ExpressionWrapper, DecimalField
from django.urls import reverse
from django.views.generic import TemplateView, DetailView

from project.enhancedsearch.views import MultiSelectWithRangeFacetedSearchView
from .models import Regione, Settore, EntePartecipato, EntePartecipatoCronologia, Categoria, EnteControllante


ABSOLUTE_RANGE = (10 * 10 ** 6, 40 * 10 ** 6)
PERCENTAGE_RANGE = (30, 60)


class AggregatoMixin(object):
    def get_search_url(self):
        search_url = '{}?q=&selected_facets=in_attivita:true'.format(reverse('entepartecipato_search'))

        if hasattr(self, 'object'):
            search_url = '{}&{}'.format(search_url, self.object.get_search_filter_querystring())

        return search_url

    def get_item_url(self, item):
        if hasattr(self, 'object'):
            if item == self.object:
                return self.get_search_url()
            elif type(item) == type(self.object):
                return item.get_absolute_url()
            else:
                return '{}&{}'.format(self.get_search_url(), item.get_search_filter_querystring())
        else:
            return item.get_absolute_url()

    def get_entepartecipatocronologia_queryset(self):
        return EntePartecipatoCronologia.objects.all()

    def get_aggregate_data(self):
        anno_rilevazione = EntePartecipatoCronologia.objects.ultimo_anno_rilevazione()

        entipartecipaticronologie = self.get_entepartecipatocronologia_queryset()
        entipartecipaticronologie_attive = entipartecipaticronologie.attive()
        entipartecipaticronologie_attive_ultimoanno = entipartecipaticronologie_attive.filter(anno_rilevazione=anno_rilevazione)

        aggregate_data = entipartecipaticronologie.aggregate(
            numero_partecipate_totali=Count('ente_partecipato', distinct=True),
        )

        aggregate_data.update(entipartecipaticronologie_attive_ultimoanno.aggregate(
            numero_partecipate_attive=Count('ente_partecipato'),
            spese_totali=Sum('spese_totali'),
            spese_personale=Sum('spese_personale'),
            spese_investimenti=Sum('spese_investimenti')
        ))

        for key in ('spese_personale', 'spese_investimenti'):
            aggregate_data['{}_perc'.format(key)] = 100 * aggregate_data[key] / aggregate_data['spese_totali']

        aggregate_data['regioni'] = Regione.objects.filter(quote__ente_partecipato_cronologia__in=entipartecipaticronologie_attive_ultimoanno).annotate(numero_partecipate=Count('quote__ente_partecipato_cronologia__ente_partecipato')).order_by('-numero_partecipate', 'denominazione')

        aggregate_data['settori'] = Settore.objects.filter(quote__ente_partecipato_cronologia__in=entipartecipaticronologie_attive_ultimoanno).annotate(numero_partecipate=Count('quote__ente_partecipato_cronologia__ente_partecipato')).order_by('-numero_partecipate', 'denominazione')

        for obj in list(aggregate_data['regioni']) + list(aggregate_data['settori']):
            obj.url = self.get_item_url(obj)

        aggregate_data['sottotipi'] = Categoria.objects.filter(gruppo='sottotipo', ente_partecipato_by_sottotipo__cronologia__in=entipartecipaticronologie_attive_ultimoanno).annotate(numero_partecipate=Count('ente_partecipato_by_sottotipo'), governance=StringAgg('ente_partecipato_by_sottotipo__governance__denominazione', delimiter=' ', distinct=True, ordering='ente_partecipato_by_sottotipo__governance__denominazione'))

        aggregate_data['categorie'] = Categoria.objects.filter(gruppo='categoria', ente_partecipato_by_categoria__cronologia__in=entipartecipaticronologie_attive_ultimoanno).annotate(numero_partecipate=Count('ente_partecipato_by_categoria'), governance=StringAgg('ente_partecipato_by_categoria__governance__denominazione', delimiter=' ', distinct=True, ordering='ente_partecipato_by_categoria__governance__denominazione'))

        anni_rilevazione = [str(x) for x in range(2000, int(anno_rilevazione) + 1)]

        for attr in ('entrate_totali', 'spese_totali', 'spese_personale', 'spese_investimenti'):
            qs = entipartecipaticronologie_attive
            if attr in ('entrate_totali', 'spese_totali'):
                fld = attr
                fldrange = ABSOLUTE_RANGE
            else:
                qs = qs.annotate(percentage=ExpressionWrapper(100 * F(attr) / F('spese_totali'), output_field=DecimalField()))
                fld = 'percentage'
                fldrange = PERCENTAGE_RANGE
            valori_per_cluster = {cluster: dict(qs.filter(**filters).order_by('anno_rilevazione').values_list('anno_rilevazione').annotate(numero_partecipate=Count('ente_partecipato'))) for cluster, filters in (('cluster1', {'{}__lt'.format(fld): fldrange[0]}), ('cluster2', {'{}__gte'.format(fld): fldrange[0], '{}__lt'.format(fld): fldrange[1]}), ('cluster3', {'{}__gte'.format(fld): fldrange[1]}))}

            valori_per_anno = []
            for anno in anni_rilevazione:
                totale = sum(valori.get(anno, 0) for valori in valori_per_cluster.values())
                valori_per_anno.append({'anno': anno, **{cluster: round(100 * valori.get(anno, 0) / totale if totale else 0, 0) for cluster, valori in valori_per_cluster.items()}})

            aggregate_data['{}_per_anno'.format(attr)] = valori_per_anno

        entipartecipati_creati_per_anno = dict(entipartecipaticronologie.filter(stato='N').values_list('anno_rilevazione').annotate(numero_partecipate=Count('ente_partecipato')).order_by('anno_rilevazione'))
        entipartecipati_cessati_per_anno = dict(entipartecipaticronologie.filter(stato='C').values_list('anno_rilevazione').annotate(numero_partecipate=Count('ente_partecipato')).order_by('anno_rilevazione'))

        entipartecipati_per_anno = []
        for anno in anni_rilevazione[1:]:
            entipartecipati_per_anno.append({'anno': anno, 'creati': entipartecipati_creati_per_anno.get(anno, 0), 'cessati': entipartecipati_cessati_per_anno.get(anno, 0)})

        aggregate_data['entipartecipati_per_anno'] = entipartecipati_per_anno

        aggregate_data['entipartecipati_top'] = entipartecipaticronologie_attive_ultimoanno.filter(spese_totali__isnull=False).order_by('-spese_totali', 'ente_partecipato__denominazione').select_related('ente_partecipato')[:10]

        aggregate_data['enticontrollanti_top'] = EnteControllante.objects.filter(quote__ente_partecipato_cronologia__in=entipartecipaticronologie_attive_ultimoanno).annotate(numero_partecipate=Count('quote__ente_partecipato_cronologia__ente_partecipato')).order_by('-numero_partecipate', 'denominazione')[:10]

        aggregate_data['anno_rilevazione'] = anno_rilevazione

        aggregate_data['anni_range'] = (anni_rilevazione[0], anni_rilevazione[-1])

        aggregate_data['search_url'] = self.get_search_url()

        return aggregate_data


class HomeView(AggregatoMixin, TemplateView):
    template_name = 'core/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'home'

        context.update(self.get_aggregate_data())

        return context


class RegioneDetailView(AggregatoMixin, DetailView):
    model = Regione

    def get_entepartecipatocronologia_queryset(self):
        return super().get_entepartecipatocronologia_queryset().filter(regionequota__regione=self.object)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'regioni'

        context.update(self.get_aggregate_data())

        return context


class SettoreDetailView(AggregatoMixin, DetailView):
    model = Settore

    def get_entepartecipatocronologia_queryset(self):
        return super().get_entepartecipatocronologia_queryset().filter(settorequota__settore=self.object)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'settori'

        context.update(self.get_aggregate_data())

        return context


class EntePartecipatoDetailView(DetailView):
    model = EntePartecipato

    def get_queryset(self):
        return super().get_queryset().select_related('sottotipo', 'governance').prefetch_related('cronologia__regionequota_set__regione', 'cronologia__settorequota_set__settore', 'cronologia__entecontrollantequota_set__ente_controllante__tipologia', 'cronologia__entecontrollatoquota_set__ente_controllato')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'partecipate'

        context['anni_rilevazione'] = [o.anno_rilevazione for o in self.object.cronologie_attive]

        context['anno_rilevazione'] = self.request.GET.get('anno')
        if context['anno_rilevazione'] not in context['anni_rilevazione']:
            context['anno_rilevazione'] = context['anni_rilevazione'][-1]

        self.object.cronologia_corrente = [o for o in self.object.cronologie_attive if o.anno_rilevazione == context['anno_rilevazione']][0]

        context['tipologie_enticontrollanti'] = Categoria.objects.filter(gruppo='tipologia_entecontrollante')

        return context


class EnteControllanteDetailView(DetailView):
    model = EnteControllante

    def get_queryset(self):
        return super().get_queryset().select_related('tipologia')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'controllanti'

        anno_rilevazione = EntePartecipatoCronologia.objects.ultimo_anno_rilevazione()

        entipartecipaticronologie = EntePartecipatoCronologia.objects.attive().filter(anno_rilevazione=anno_rilevazione, entecontrollantequota__ente_controllante=self.object)

        context['numero_partecipate'] = entipartecipaticronologie.count()

        context['partecipate_per_spese_totali'] = entipartecipaticronologie.filter(spese_totali__isnull=False).order_by('-spese_totali').select_related('ente_partecipato')[:10]

        context['partecipate_per_entrate_totali'] = entipartecipaticronologie.filter(entrate_totali__isnull=False).order_by('-entrate_totali').select_related('ente_partecipato')[:10]

        for attr in ('spese_investimenti', 'spese_personale'):
            context['partecipate_per_perc_{}'.format(attr)] = entipartecipaticronologie.filter(spese_totali__isnull=False, **{'{}__isnull'.format(attr): False}).annotate(percentage=ExpressionWrapper(100 * F(attr) / F('spese_totali'), output_field=DecimalField())).order_by('-percentage').select_related('ente_partecipato')[:10]

        return context


class EntePartecipatoSearchView(MultiSelectWithRangeFacetedSearchView):
    template_name = 'search/search.html'

    model = EntePartecipato

    FACETS = ('in_attivita', 'regione', 'settore', 'categoria', 'sottotipo')

    abs_range = {
        '0': {
            'qrange': '[{} TO *]'.format(ABSOLUTE_RANGE[1]),
            'label': 'Grande',
        },
        '1': {
            'qrange': '[{} TO {}}}'.format(ABSOLUTE_RANGE[0], ABSOLUTE_RANGE[1]),
            'label': 'Media',
        },
        '2': {
            'qrange': '[* TO {}}}'.format(ABSOLUTE_RANGE[0]),
            'label': 'Piccola',
        },
    }

    perc_range = {
        '0': {
            'qrange': '[{} TO *]'.format(PERCENTAGE_RANGE[1]),
            'label': 'Elevata',
        },
        '1': {
            'qrange': '[{} TO {}}}'.format(PERCENTAGE_RANGE[0], PERCENTAGE_RANGE[1]),
            'label': 'Media',
        },
        '2': {
            'qrange': '[* TO {}}}'.format(PERCENTAGE_RANGE[0]),
            'label': 'Ridotta',
        },
    }

    RANGES = {
        'entrate_totali': abs_range,
        'spese_totali': abs_range,
        'spese_investimenti_perc': perc_range,
        'spese_personale_perc': perc_range,
    }

    SORT_CHOICES = {
        '0': {
            'fields': ['denominazione_slug'],
            'display': 'Denominazione',
        },
        '1': {
            'fields': ['-spese_totali', 'denominazione_slug'],
            'display': 'Dimensione della spesa',
        },
    }

    @staticmethod
    def _get_objects_by_pk(pks):
        return {str(key): value for key, value in EntePartecipato.objects.select_related('categoria', 'sottotipo').prefetch_related('cronologia__regionequota_set__regione', 'cronologia__settorequota_set__settore').in_bulk(pks).items()}

    def get_queryset(self):
        self.kwargs['orderby'] = self.request.GET.get('orderby')
        if self.kwargs['orderby'] not in self.SORT_CHOICES:
            self.kwargs['orderby'] = next(iter(self.SORT_CHOICES))
        return super().get_queryset().order_by(*self.SORT_CHOICES[self.kwargs['orderby']]['fields'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'partecipate'

        context['model_name'] = self.model.__name__.lower()

        context['my_facets'] = [
            self._build_facet_field_info('regione', 'Regione di attività', {o.slug: o.denominazione for o in Regione.objects.all()}),
            self._build_facet_field_info('settore', 'Settore di attività', {o.slug: o.denominazione for o in Settore.objects.all()}),
            self._build_facet_field_info('categoria', 'Categoria', {o.slug: o.denominazione for o in Categoria.objects.filter(gruppo='categoria')}),
            self._build_facet_field_info('sottotipo', 'Sottotipo', {o.slug: o.denominazione for o in Categoria.objects.filter(gruppo='sottotipo')}),
            self._build_range_facet_queries_info('entrate_totali', 'Dimensione delle entrate'),
            self._build_range_facet_queries_info('spese_totali', 'Dimensione della spesa'),
            self._build_range_facet_queries_info('spese_personale_perc', 'Spesa per il personale'),
            self._build_range_facet_queries_info('spese_investimenti_perc', 'Spesa per gli investimenti'),
            self._build_facet_field_info('in_attivita', 'Stato di attività', {'true': 'In attività', 'false': 'Cessata'}),
        ]

        params = self.params
        context['orderby_choices'] = []
        for choice_value, choice in self.SORT_CHOICES.items():
            params['orderby'] = choice_value
            context['orderby_choices'].append({
                'display': choice['display'],
                'query_string': params.urlencode(safe=':'),
                'selected': bool(choice_value == self.kwargs['orderby']),
            })

        return context


class EnteControllanteSearchView(MultiSelectWithRangeFacetedSearchView):
    template_name = 'search/search.html'

    model = EnteControllante

    FACETS = ('tipologia',)

    @staticmethod
    def _get_objects_by_pk(pks):
        anno_rilevazione = EntePartecipatoCronologia.objects.ultimo_anno_rilevazione()
        return {str(key): value for key, value in EnteControllante.objects.filter(quote__ente_partecipato_cronologia__anno_rilevazione=anno_rilevazione).annotate(numero_partecipate=Count('quote__ente_partecipato_cronologia__ente_partecipato')).select_related('tipologia').in_bulk(pks).items()}

    def get_queryset(self):
        return super().get_queryset().order_by('denominazione_slug')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['section'] = 'controllanti'

        context['model_name'] = self.model.__name__.lower()

        context['my_facets'] = [
            self._build_facet_field_info('tipologia', 'Tipologia', {o.slug: o.denominazione for o in Categoria.objects.filter(gruppo='tipologia_entecontrollante')}),
        ]

        return context
