if (!window.highcharts_presets) window.highcharts_presets = {};

window.highcharts_presets["default"] = {
    chart: {
        backgroundColor: 'transparent'
    },
    title: {
        text: ''
    },
    credits: {
        enabled: false
    },
    legend: {
        enabled: false
    },
    exporting: {
        enabled: false
    },
    xAxis: {
        lineWidth: 0,
        gridLineWidth: 0,
        minorGridLineWidth: 0,
        categories: []
    },
    yAxis: {
        min: 0,
        lineWidth: 0,
        gridLineWidth: 0,
        minorGridLineWidth: 0,
        title: {
            enabled: false
        }
    },
    tooltip: {
        shared: true,
        outside: true,
        animation: false,
        backgroundColor: '#FFF',
        borderColor: '#1990C2',
        borderRadius: 8,
        borderWidth: 2,
        padding: 10,
        distance: 20,
        style: {
            fontSize: '14px'
        }
    }
};

window.highcharts_presets["multi-line"] = {
    chart: {
        type: 'line',
        height: 160
    },
    colors: [
        '#04003B',
        '#2290C2',
        '#62C8BB'
    ],
    xAxis: {
        labels: {
            rotation: 0,
            step: 2
        }
    },
    yAxis: {
        gridLineWidth: 1
    }
};

window.highcharts_presets["line"] = {
    chart: {
        type: 'line',
        height: 160
    },
    colors: [
        '#2290C2'
    ],
    xAxis: {
        labels: {
            rotation: 0,
            step: 2
        }
    },
    yAxis: {
        gridLineWidth: 1
    }
};

window.highcharts_presets["stacked-bar"] = {
    chart: {
        type: 'bar',
        height: 60
    },
    colors: [
        '#04003B',
        '#2290C2',
        '#62C8BB'
    ],
    xAxis: {
        labels: {
            enabled: false
        }
    },
    yAxis: {
        labels: {
            enabled: false
        }
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            borderWidth: 0,
            pointWidth: 32,
            dataLabels: {
                enabled: true,
                align: 'left',
                overflow: 'allow',
                crop: false,
                y: 24,
                shadow: false,
                color: '#000',
                style: {
                    fontSize: '12px',
                    fontWeight: 'normal',
                    textOutline: 0
                }
            }
        }
    }
};

window.highcharts_presets["bar-percent"] = {
    chart: {
        type: 'bar',
        height: 60,
        plotBackgroundColor: '#FFF'
    },
    colors: [
        '#2290C2'
    ],
    xAxis: {
        labels: {
            enabled: false
        }
    },
    yAxis: {
        max: 100,
        labels: {
            enabled: false
        }
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            borderWidth: 0,
            pointWidth: 35,
            dataLabels: {
                enabled: true,
                align: 'left',
                shadow: false,
                color: '#333',
                style: {
                    fontSize: '16px',
                    fontWeight: 'normal',
                    textOutline: 0
                }
            }
        }
    }
};

window.highcharts_presets["column"] = {
    chart: {
        type: 'column',
        height: 230
    },
    colors: [
        '#04003B',
        '#2290C2'
    ],
    xAxis: {
        labels: {
            rotation: 0,
            style: {
                fontSize: '14px',
                fontWeight: 'normal',
                textOutline: 0
            }
        }
    },
    yAxis: {
        gridLineWidth: 1,
        labels: {
            enabled: false
        }
    },
    tooltip: {
        pointFormat: '<span style="font-weight: bold; color: #1990c2">{point.y}</span><br/>',
        headerFormat: '<span style="font-weight: bold; font-size: 14px">{point.key}</span><br/>'
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            borderWidth: 0,
            pointWidth: 50,
            dataLabels: {
                enabled: true,
                align: 'center',
                verticalAlign: 'top',
                y: -30,
                crop: false,
                overflow: 'allow',
                shadow: false,
                color: '#333',
                style: {
                    fontSize: '16px',
                    fontWeight: 'normal',
                    textOutline: 0
                }
            }
        }
    }
};

window.highcharts_presets["chart-bar"] = {
    chart: {
        type: 'column',
        height: 300,
        margin: 0,
        inverted: true,
        animation: false,
        style: {
            fontFamily: ''
        },
        events: {
            load: function() {
                var ticks = this.xAxis['0'].ticks;
                var data = this.series['0'].data;
                var len = data.length;
                for (var i = 0; i < len; i++) {
                    ticks[i].label.css({right: (data[i].dataLabel.width + 10) + 'px'});
                }
            }
        }
    },
    colors: [
        '#E1E1E1'
    ],
    xAxis: {
        labels: {
            x: 10,
            align: 'left',
            useHTML: true,
            style: {
                width: null,
                textOverflow: 'ellipsis',
                color: '#333',
                fontSize: '14px'
            }
        }
    },
    yAxis: {
        visible: false,
    },
    tooltip: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            maxPointWidth: 22,
            groupPadding: 0,
            dataLabels: {
                enabled: true,
                color: '#1990C2',
                useHTML: true,
                formatter: fmtNumber,
                style: {
                    fontSize: '14px'
                }
            },
            states: {
                hover: {
                    enabled: false
                }
            },
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        location.href = $(this.name).attr('href');
                    }
                }
            }
        }
    }
};

window.highcharts_presets["tree"] = {
    chart: {
        type: 'treemap',
        margin: 0,
        spacing: 0
    },
    xAxis: null,
    yAxis: null,
    plotOptions: {
        series: {
            layoutAlgorithm: 'squarified',
            dataLabels: {
                style: {
                    color: '#FFF',
                    fontWeight: 'bold',
                    textOutline: 0,
                    textTransform: 'uppercase'
                }
            },
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        location.href = this.href;
                    }
                }
            }
        }
    }
};

function fmtNumber() {
    if (this.y >= Math.pow(10, 9))
        return Highcharts.numberFormat(this.y / Math.pow(10, 9), 1) + ' mld';
    else if (this.y >= Math.pow(10, 6))
        return Highcharts.numberFormat(this.y / Math.pow(10, 6), 1) + ' mln';
    else
        return Highcharts.numberFormat(this.y, Number.isInteger(this.y) ? 0 : 2);
}
