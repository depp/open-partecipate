from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.urls import include, path
from django.views import defaults as default_views

from project.core.views import HomeView, RegioneDetailView, SettoreDetailView, EntePartecipatoDetailView, \
    EnteControllanteDetailView, EntePartecipatoSearchView, EnteControllanteSearchView

urlpatterns = [
    path(settings.ADMIN_URL, admin.site.urls),

    path('', login_required(HomeView.as_view()), name='home'),
    path('regioni/<slug:slug>/', login_required(RegioneDetailView.as_view()), name='regione_detail'),
    path('settori/<slug:slug>/', login_required(SettoreDetailView.as_view()), name='settore_detail'),
    path('partecipate/', login_required(EntePartecipatoSearchView.as_view()), name='entepartecipato_search'),
    path('partecipate/<int:pk>/', login_required(EntePartecipatoDetailView.as_view()), name='entepartecipato_detail'),
    path('partecipanti/', login_required(EnteControllanteSearchView.as_view()), name='entecontrollante_search'),
    path('partecipanti/<int:pk>/', login_required(EnteControllanteDetailView.as_view()), name='entecontrollante_detail'),

    path('accounts/login/', LoginView.as_view(), name='login'),

    path('api/', include('project.core.api.urls')),

    # ckeditor uploader
    path('ckeditor/', include('ckeditor_uploader.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path('400/', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        path('403/', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        path('404/', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        path('500/', default_views.server_error),
    ]

    if settings.DEBUG_TOOLBAR:
        import debug_toolbar
        urlpatterns.append(path('__debug__/', include(debug_toolbar.urls)))
