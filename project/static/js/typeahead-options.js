if(!window.typeahead_presets) window.typeahead_presets = {};
typeahead_presets = {
    maxItem: false,
    highlight: false,
    mustSelectItem: true,
    cancelButton: false,
    filter: false,
    minLength: 0,
    searchOnFocus: true,
    source: {
        entepartecipato: {
            data: function() {
                return [this.node.data('entepartecipato-texts').split('|')[0]];
            },
            href: function(item) {
                return this.generateHref(this.node.data('entepartecipato-href'), item);
            }
        },
        entecontrollante: {
            data: function() {
                return [this.node.data('entecontrollante-texts').split('|')[0]];
            },
            href: function(item) {
                return this.generateHref(this.node.data('entecontrollante-href'), item);
            }
        }
    },
    callback: {
        onNavigateBefore: function (node, query, event) {
            if (~[38,40].indexOf(event.keyCode)) {
                event.preventInputChange = true;
            }
        },
        onHideLayout: function (node, query) {
            node.val('');
        },
        onLayoutBuiltBefore: function (node, query, result, resultHtmlList) {
            var scope = this;

            $.each(['entepartecipato', 'entecontrollante'], function (index, item) {
                resultHtmlList.find('li[data-search-group="' + item + '"]').remove();

                if (query) {
                    var href = scope.generateHref(node.data(item + '-href'), {query: query});
                    var text = node.data(item + '-texts').split('|')[1].replace('{{query}}', '"' + query + '"');

                    resultHtmlList.find('li[data-group="' + item + '"]').find('a').attr('href', href).find('span').text(text);
                }
            });

            return resultHtmlList;
        }
    }
};
