from rest_framework import serializers
from rest_framework.reverse import reverse

from ..models import EntePartecipato, EntePartecipatoCronologia, RegioneQuota, SettoreQuota, EnteControllanteQuota, EnteControllatoQuota, Regione, Settore


class AggregationURLField(serializers.Field):
    url_name = None

    def __init__(self, identifier):
        self.url_name = 'api-aggregati-{}-detail'.format(identifier)
        super().__init__(self, source='*')

    def to_representation(self, obj):
        request = self.context.get('request')
        format = self.context.get('format')

        return reverse(self.url_name, kwargs={'slug': obj.slug}, request=request, format=format)


class FilterURLField(serializers.Field):
    param_name = None

    def __init__(self, identifier):
        self.param_name = identifier
        super().__init__(self, source='*')

    def to_representation(self, obj):
        import urllib.parse as urlparse

        request = self.context.get('request')
        format = self.context.get('format')

        url = reverse('api-entepartecipato-list', request=request, format=format)
        url_parts = urlparse.urlsplit(url)
        query_parts = urlparse.parse_qsl(url_parts.query)
        query_parts.append((self.param_name, obj.slug))
        url_parts = url_parts._replace(query=urlparse.urlencode(query_parts))

        return url_parts.geturl()


class QuotaSerializer(serializers.Serializer):
    fld_name = None

    def __init__(self, identifier, *args, **kwargs):
        self.fld_name = identifier
        super().__init__(self, *args, **kwargs)

    def to_representation(self, obj):
        return {
            'denominazione': serializers.StringRelatedField().to_representation(getattr(obj, self.fld_name)),
            'quota': serializers.DecimalField(max_digits=5, decimal_places=2).to_representation(obj.quota),
        }


class EntePartecipatoCronologiaModelSerializer(serializers.ModelSerializer):
    stato = serializers.CharField(source='get_stato_display')
    settori = QuotaSerializer(identifier='settore', source='settorequota_set', many=True)
    regioni = QuotaSerializer(identifier='regione', source='regionequota_set', many=True)
    enti_controllanti = QuotaSerializer(identifier='ente_controllante', source='entecontrollantequota_set', many=True)
    enti_controllati = QuotaSerializer(identifier='ente_controllato', source='entecontrollatoquota_set', many=True)

    class Meta:
        model = EntePartecipatoCronologia
        fields = ['anno_rilevazione', 'stato', 'quotato', 'dipendenti', 'entrate_totali', 'spese_totali', 'spese_personale', 'spese_investimenti', 'numero_unita_locali', 'livello_partecipazione', 'settori', 'regioni', 'enti_controllanti', 'enti_controllati']


class EntePartecipatoDetailModelSerializer(serializers.ModelSerializer):
    sottotipo = serializers.StringRelatedField()
    tipologia = serializers.StringRelatedField()
    governance = serializers.StringRelatedField()
    categoria = serializers.StringRelatedField()
    cronologia = EntePartecipatoCronologiaModelSerializer(many=True)

    class Meta:
        model = EntePartecipato
        fields = ['denominazione', 'codice_fiscale', 'provincia', 'comune', 'indirizzo', 'anno_inizio_attivita', 'sottotipo', 'tipologia', 'governance', 'categoria', 'cronologia']


class EntePartecipatoListModelSerializer(serializers.ModelSerializer):
    entrate_totali = serializers.IntegerField(source='ultima_cronologia.entrate_totali')
    spese_totali = serializers.IntegerField(source='ultima_cronologia.spese_totali')
    spese_personale = serializers.IntegerField(source='ultima_cronologia.spese_personale')
    spese_investimenti = serializers.IntegerField(source='ultima_cronologia.spese_investimenti')
    categoria = serializers.StringRelatedField()
    sottotipo = serializers.StringRelatedField()
    settori = QuotaSerializer(identifier='settore', source='ultima_cronologia.settorequota_set', many=True)
    regioni = QuotaSerializer(identifier='regione', source='ultima_cronologia.regionequota_set', many=True)

    class Meta:
        model = EntePartecipato
        fields = ['url', 'denominazione', 'in_attivita', 'entrate_totali', 'spese_totali', 'spese_personale', 'spese_investimenti', 'categoria', 'sottotipo', 'settori', 'regioni']
        extra_kwargs = {
            'url': {'view_name': 'api-entepartecipato-detail', 'lookup_field': 'id'},
        }


class EntePartecipatoSearchResultSerializer(serializers.BaseSerializer):
    def to_representation(self, obj):
        return EntePartecipatoListModelSerializer(context={'request': self.context.get('request')}).to_representation(obj.object)


class RegioneDetailModelSerializer(serializers.ModelSerializer):
    dati_aggregati = AggregationURLField('regione')
    lista_partecipate = FilterURLField('regione')

    class Meta:
        model = Regione
        fields = ['denominazione', 'slug', 'dati_aggregati', 'lista_partecipate']


class RegioneListModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Regione
        fields = ('url', 'denominazione', 'slug')
        extra_kwargs = {
            'url': {'view_name': 'api-regione-detail', 'lookup_field': 'slug'},
        }


class SettoreDetailModelSerializer(serializers.ModelSerializer):
    dati_aggregati = AggregationURLField('settore')
    lista_partecipate = FilterURLField('settore')

    class Meta:
        model = Settore
        fields = ['denominazione', 'slug', 'dati_aggregati', 'lista_partecipate']


class SettoreListModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Settore
        fields = ('url', 'denominazione', 'slug')
        extra_kwargs = {
            'url': {'view_name': 'api-settore-detail', 'lookup_field': 'slug'},
        }
