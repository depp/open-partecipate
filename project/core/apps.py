from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'project.core'
    verbose_name = 'Open Partecipate'
