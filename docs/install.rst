.. _install:

Install
=========

This is how to get a new laptop run this project.

Development is performed on local workstations, without requiring
Docker.

Environment variables are read from from the ``.env`` file.
This file is not present in the repository,
and it should be generated starting from ``config/samples/.env``.
Variables in the linux environent at execution time override those
read from ``.env``.

A running postgis database is required somewhere.
Connections to these server should be defined in the ``.env`` file, as
``DATABASE_URL`` value.

To start developing, clone this repository, then:

.. code-block:: bash

    # generate (and modify) the .env file
    cp config/samples/.env .env

    # create and activate a virtualenv
    python3 -m venv venv
    source venv/bin/activate

    # install requirements
    pip install -r requirements.txt

    # install other useful packages (docs-writing, ipython, jupyter ...)
    pip install sphinx sphinx_rtd_theme

    # create database
    createdb -Upostgres politiche_2018

    python manage.py migrate
    python manage.py createsuperuser

    # now the server can be run
    python manage.py runserver



.. _tmuxinator:

Restore development session
---------------------------

``tmuxinator`` can be used to restore a development session.
The ``.tmuxinator.yml`` file contains all information needed to start
the session.


