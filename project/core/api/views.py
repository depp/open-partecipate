from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from ..models import EntePartecipato, Regione, Settore
from ..views import EntePartecipatoSearchView
from .serializers import EntePartecipatoSearchResultSerializer, EntePartecipatoDetailModelSerializer, \
    RegioneListModelSerializer, RegioneDetailModelSerializer, SettoreListModelSerializer, SettoreDetailModelSerializer


class ApiRootView(APIView):
    """
    Questo è il punto di accesso delle API di OpenPartecipate.

    Le API sono in sola lettura, liberamente accessibili attraverso richieste HTTP.

    Le risposte sono fornite sia in formato **HTML navigabile** che in formato **JSON**.
    """

    # To serve all requests and avoid slow responses or downtimes due to misuse, we limit the requests rate.
    # When accessing the API **anonymously**, your client is limited to **12 requests per minute** from the same IP.
    # You can contact us to become an **authenticated API user** (it's still free),
    # then the rate-limit would be lifted to **1 request per second**.
    #
    # Authentication is done through HTTP Basic Authentication.

    name = 'Indice API'

    def get(self, request, format=None):
        return Response({
            'partecipate': reverse('api-entepartecipato-list', request=request, format=format),
            'regioni': reverse('api-regione-list', request=request, format=format),
            'settori': reverse('api-settore-list', request=request, format=format),
            'dati_aggregati': reverse('api-aggregati-home', request=request, format=format),
        })


class BaseViewSet(viewsets.ReadOnlyModelViewSet):
    def get_serializer_class(self):
        serializer_class = super().get_serializer_class()
        if self.detail:
            serializer_class = globals()[serializer_class.__name__.replace('ListModel', 'DetailModel')]
        return serializer_class

    def get_view_name(self):
        model_meta = self.get_queryset().model._meta
        if self.detail:
            return 'Dettaglio {}'.format(model_meta.verbose_name)
        else:
            return 'Elenco {}'.format(model_meta.verbose_name_plural)

    def get_view_description(self, html=False):
        if self.detail:
            return ''
        return super().get_view_description(html)


class BaseSearchViewSet(BaseViewSet):
    def _get_objects_by_pk(self, pks):
        return {}

    def _hydrate(self, object_list):
        objects_by_pk = self._get_objects_by_pk(object.pk for object in object_list)

        for object in object_list:
            try:
                object.object = objects_by_pk[object.pk]
            except KeyError:
                pass

        return object_list

    def _get_facet_counts(self):
        raise NotImplementedError('{}._get_facet_counts() must be implemented'.format(self.__class__.__name__))

    def get_searchqueryset(self):
        raise NotImplementedError('{}.get_searchqueryset() must be implemented'.format(self.__class__.__name__))

    def get_serializer_class(self):
        serializer_class = super().get_serializer_class()
        if self.detail:
            serializer_class = globals()[serializer_class.__name__.replace('SearchResult', 'DetailModel')]
        return serializer_class

    def list(self, request, *args, **kwargs):
        queryset = self.get_searchqueryset()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(self._hydrate(page), many=True)
            response = self.get_paginated_response(serializer.data)
            response.data['facet_counts'] = self._get_facet_counts()
            response.data.move_to_end('results')
            return response

        serializer = self.get_serializer(self._hydrate(queryset), many=True)
        return Response({
            'facet_counts': self._get_facet_counts(),
            'results': serializer.data,
        })


class EntePartecipatoViewSet(BaseSearchViewSet):
    """
    L'elenco è paginato a 10 elementi per pagina.

    Le pagine possono essere navigate tramite i link "next" e "previous".

    La scheda di dettaglio può essere raggiunta tramite il link "url" di ogni elemento della lista.

    L'elenco può essere filtrato tramite i parametri GET ``regione``, ``settore``.
    I filtri utilizzano gli slug. I valori degli slug possono essere ricavati ai seguenti indirizzi:

    * ``/api/regioni/``
    * ``/api/settori/``

    L'elenco può anche essere filtrato tramite il parametro GET ``in_attivita``, che può assumere i valori ``true`` o ``false``.

    Possono essere impostati filtri multipli.

    L'elenco è ordinato di default per ``denominazione`` crescente.
    È possibile cambiare l'ordinamento tramite il parametro GET ``orderby``.
    I valori possibili sono:

    * ``0`` (default), ``denominazione`` crescente
    * ``1``, ``spese_totali`` decrescente

    Esempi
    ========

    * ``/api/partecipate/?regione=abruzzo``
    * ``/api/partecipate/?settore=agricoltura``
    * ``/api/partecipate/?regione=abruzzo&settore=agricoltura``
    * ``/api/partecipate/?in_attivita=true&orderby=1``
    """

    serializer_class = EntePartecipatoSearchResultSerializer
    queryset = EntePartecipato.objects.select_related('categoria', 'sottotipo').prefetch_related('cronologia__regionequota_set__regione', 'cronologia__settorequota_set__settore')
    lookup_field = 'id'

    def _get_objects_by_pk(self, pks):
        return {str(key): value for key, value in self.get_queryset().in_bulk(pks).items()}

    def _get_facet_counts(self):
        facet_counts = self.get_searchqueryset().facet_counts()

        facets_cod2slug = [
            ('regione', {x['slug']: x['denominazione'] for x in Regione.objects.values('slug', 'denominazione')}),
            ('settore', {x['slug']: x['denominazione'] for x in Settore.objects.values('slug', 'denominazione')}),
            ('in_attivita', {'true': 'In attività', 'false': 'Cessata'}),
        ]

        return {
            facet_name: ['{} ({})'.format(facet_cod2slug[x[0]], x[1]) for x in facet_counts['fields'][facet_name] if x[1] and x[0] in facet_cod2slug]
            for facet_name, facet_cod2slug in facets_cod2slug if facet_name in facet_counts['fields']
        }

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.detail:
            queryset = queryset.select_related('tipologia', 'governance').prefetch_related('cronologia__entecontrollantequota_set__ente_controllante', 'cronologia__entecontrollatoquota_set__ente_controllato')
        return queryset

    def get_searchqueryset(self):
        ret_sqs = EntePartecipatoSearchView(request=self.request, kwargs=self.kwargs).get_queryset()

        for fld in ('regione', 'settore', 'in_attivita'):
            val = self.request.query_params.get(fld)
            if val:
                ret_sqs = ret_sqs.filter(**{fld: val})

        return ret_sqs


class RegioneViewSet(BaseViewSet):
    """
    L'elenco è paginato a 10 elementi per pagina.

    Le pagine possono essere navigate tramite i link "next" e "previous".

    La scheda di dettaglio può essere raggiunta tramite il link "url" di ogni elemento della lista.
    """

    serializer_class = RegioneListModelSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        return Regione.objects.all()


class SettoreViewSet(BaseViewSet):
    """
    L'elenco è paginato a 10 elementi per pagina.

    Le pagine possono essere navigate tramite i link "next" e "previous".

    La scheda di dettaglio può essere raggiunta tramite il link "url" di ogni elemento della lista.
    """

    serializer_class = SettoreListModelSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        return Settore.objects.all()


class BaseAggregatoView(APIView):
    def get_aggregate_page_url(self):
        raise NotImplementedError('{}.get_aggregate_page_url() must be implemented'.format(self.__class__.__name__))

    def _get_aggregate_page_context(self, *args, **kwargs):
        from django.test import RequestFactory
        from django.urls import resolve

        url = self.get_aggregate_page_url()

        resolver = resolve(url)

        view = resolver.func.view_class()

        view.request = RequestFactory().get(url)
        view.args = resolver.args
        view.kwargs = resolver.kwargs

        if hasattr(view, 'get_object'):
            view.object = view.get_object()
        elif hasattr(view, 'get_queryset'):
            view.object_list = view.get_queryset()

        return view.get_context_data(*args, **kwargs)

    def get_extra_context(self, object):
        return None

    def get_aggregate_data(self, request, format=None, *args, **kwargs):
        context = self._get_aggregate_page_context(*args, **kwargs)

        aggregate_data = {'contesto': self.get_extra_context(context.get('object'))}

        for key in ('anno_rilevazione', 'numero_partecipate_totali', 'numero_partecipate_attive', 'spese_totali', 'spese_personale', 'spese_investimenti'):
            aggregate_data[key] = context[key]

        for key in ('regioni', 'settori'):
            aggregate_data[key] = [{
                'denominazione': o.denominazione,
                'link': reverse('api-aggregati-{}e-detail'.format(key[0:-1]), request=request, format=format, kwargs={'slug': o.slug}),
                'numero_partecipate': o.numero_partecipate,
            } for o in context[key]]

        for key in ('sottotipi', 'categorie'):
            aggregate_data[key] = [{
                'denominazione': o.denominazione,
                'numero_partecipate': o.numero_partecipate,
                'governance': o.governance.split(' '),
            } for o in context[key]]

        for key in ('entrate_totali_per_anno', 'spese_totali_per_anno', 'spese_personale_per_anno', 'spese_investimenti_per_anno'):
            aggregate_data[key] = [{{'cluster1': 'piccola', 'cluster2': 'media', 'cluster3': 'elevata'}.get(k, k): v for k, v in x.items()} for x in context[key]]

        aggregate_data['entipartecipati_per_anno'] = context['entipartecipati_per_anno']

        aggregate_data['entipartecipati_top'] = [{
            'denominazione': o.ente_partecipato.denominazione,
            'spese_totali': o.spese_totali,
        } for o in context['entipartecipati_top']]

        aggregate_data['enticontrollanti_top'] = [{
            'denominazione': o.denominazione,
            'numero_partecipate': o.numero_partecipate,
        } for o in context['enticontrollanti_top']]

        return aggregate_data

    def get(self, request, format=None, *args, **kwargs):
        return Response(self.get_aggregate_data(request, format, *args, **kwargs))


class AggregatoHomeView(BaseAggregatoView):
    name = 'Dati aggregati totali'

    def get_aggregate_page_url(self):
        return reverse('home')

    def get_extra_context(self, object):
        return {'tipo': 'tutto'}


class AggregatoRegioneDetailView(BaseAggregatoView):
    name = 'Dati aggregati regione'

    def get_aggregate_page_url(self):
        return reverse('regione_detail', kwargs={'slug': self.kwargs['slug']})

    def get_extra_context(self, object):
        return {'tipo': 'regione', 'denominazione': str(object)}


class AggregatoSettoreDetailView(BaseAggregatoView):
    name = 'Dati aggregati settore'

    def get_aggregate_page_url(self):
        return reverse('settore_detail', kwargs={'slug': self.kwargs['slug']})

    def get_extra_context(self, object):
        return {'tipo': 'settore', 'denominazione': str(object)}
