import urllib.parse
from django.db import models
from django.urls import reverse
from django.utils.functional import cached_property
from django_extensions.db.fields import AutoSlugField


# class Provincia(models.Model):
#     sigla = models.CharField(max_length=2, primary_key=True)
#     denominazione = models.CharField(max_length=30, unique=True)
#
#     def __str__(self):
#         return self.denominazione
#
#     class Meta:
#         ordering = ('denominazione',)


class Categoria(models.Model):
    GRUPPO_CHOICES = (
        ('categoria', 'Categoria'),
        ('governance', 'Governance'),
        ('sottotipo', 'Sottotipo'),
        ('tipologia', 'Tipologia'),
        ('tipologia_entecontrollante', 'Tipologia Ente Controllante'),
    )

    gruppo = models.CharField(max_length=30, choices=GRUPPO_CHOICES)
    denominazione = models.CharField(max_length=256)
    slug = AutoSlugField(populate_from='denominazione', max_length=100, unique=True)

    def __str__(self):
        return self.denominazione

    class Meta:
        ordering = ('gruppo', 'denominazione')
        unique_together = ('gruppo', 'denominazione')
        verbose_name_plural = 'categorie'


class EntePartecipato(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    denominazione = models.CharField(max_length=255)
    codice_fiscale = models.CharField(max_length=16, null=True, blank=True)
    # regione = models.ForeignKey(Regione, related_name='enti_partecipati', on_delete=models.CASCADE)
    # provincia = models.ForeignKey(Provincia, related_name='enti_partecipati', on_delete=models.CASCADE, null=True, blank=True)
    provincia = models.CharField(max_length=100, null=True, blank=True)
    comune = models.CharField(max_length=100, null=True, blank=True)
    indirizzo = models.CharField(max_length=100, null=True, blank=True)
    anno_inizio_attivita = models.CharField(max_length=4)
    # anno_fine_attivita = models.CharField(max_length=4, null=True, blank=True)
    sottotipo = models.ForeignKey(Categoria, limit_choices_to={'gruppo': 'sottotipo'}, related_name='ente_partecipato_by_sottotipo', on_delete=models.CASCADE)
    tipologia = models.ForeignKey(Categoria, limit_choices_to={'gruppo': 'tipologia'}, related_name='ente_partecipato_by_tipologia', on_delete=models.CASCADE)
    governance = models.ForeignKey(Categoria, limit_choices_to={'gruppo': 'governance'}, related_name='ente_partecipato_by_governance', on_delete=models.CASCADE)
    categoria = models.ForeignKey(Categoria, limit_choices_to={'gruppo': 'categoria'}, related_name='ente_partecipato_by_categoria', on_delete=models.CASCADE)

    @cached_property
    def cronologie_attive(self):
        return [o for o in self.cronologia.all() if o.stato != 'C']

    @cached_property
    def ultima_cronologia(self):
        try:
            return self.cronologie_attive[-1]
        except IndexError:
            return None

    @property
    def in_attivita(self):
        for obj in self.cronologia.all():
            if obj.stato == 'C':
                return False
        return True

    @property
    def anno_fine_attivita(self):
        for obj in self.cronologia.all():
            if obj.stato == 'C':
                return obj.anno_rilevazione
        return None

    def get_absolute_url(self):
        return reverse('entepartecipato_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.denominazione

    class Meta:
        verbose_name_plural = 'enti partecipati'


class EntePartecipatoCronologiaQuerySet(models.QuerySet):
    def attive(self):
        return self.exclude(stato='C')

    def ultimo_anno_rilevazione(self):
        return self.values_list('anno_rilevazione', flat=True).order_by('anno_rilevazione').distinct().last()


class EntePartecipatoCronologia(models.Model):
    STATO_CHOICES = (
        ('C', 'Cessato'),
        ('N', 'Nuovo'),
        ('P', 'Presente'),
    )
    LIVELLO_PARTECIPAZIONE_CHOICES = (
        ('1', 'primo'),
        ('2', 'secondo'),
    )

    ente_partecipato = models.ForeignKey(EntePartecipato, related_name='cronologia', on_delete=models.CASCADE)
    anno_rilevazione = models.CharField(max_length=4, db_index=True)
    stato = models.CharField(max_length=1, choices=STATO_CHOICES)
    quotato = models.NullBooleanField()
    dipendenti = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    entrate_totali = models.BigIntegerField(null=True, blank=True)
    spese_totali = models.BigIntegerField(null=True, blank=True)
    spese_investimenti = models.BigIntegerField(null=True, blank=True)
    spese_personale = models.BigIntegerField(null=True, blank=True)
    numero_unita_locali = models.SmallIntegerField(null=True, blank=True)
    livello_partecipazione = models.CharField(max_length=1, choices=LIVELLO_PARTECIPAZIONE_CHOICES, null=True, blank=True)

    objects = EntePartecipatoCronologiaQuerySet.as_manager()

    @property
    def partecipazione_pubblica(self):
        return sum(o.quota for o in self.entecontrollantequota_set.all())

    @property
    def spese_investimenti_perc(self):
        return self._spese_perc(self.spese_investimenti)

    @property
    def spese_personale_perc(self):
        return self._spese_perc(self.spese_personale)

    def _spese_perc(self, spese):
        return round(100 * (spese or 0) / self.spese_totali, 1) if self.spese_totali else None

    class Meta:
        ordering = ('ente_partecipato', 'anno_rilevazione')
        unique_together = ('ente_partecipato', 'anno_rilevazione')
        index_together = [
            ('ente_partecipato', 'anno_rilevazione'),
        ]
        verbose_name_plural = 'cronologie enti partecipati'


class EnteControllante(models.Model):
    denominazione = models.CharField(max_length=255, null=True, blank=True)
    codice_fiscale = models.CharField(max_length=16, null=True, blank=True)
    tipologia = models.ForeignKey(Categoria, limit_choices_to={'gruppo': 'tipologia_entecontrollante'}, related_name='+', on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('entecontrollante_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.denominazione or self.tipologia.denominazione

    class Meta:
        verbose_name_plural = 'enti controllanti'


class EnteControllato(models.Model):
    denominazione = models.CharField(max_length=255)
    codice_fiscale = models.CharField(max_length=16, null=True, blank=True)

    def __str__(self):
        return self.denominazione

    class Meta:
        verbose_name_plural = 'enti controllati'


class Regione(models.Model):
    denominazione = models.CharField(max_length=30, unique=True)
    slug = AutoSlugField(populate_from='denominazione', max_length=30, unique=True)
    codice = models.CharField(max_length=5, unique=True)

    def get_search_filter_querystring(self):
        return urllib.parse.urlencode({'selected_facets': 'regione:{}'.format(self.slug)}, safe=':')

    def get_absolute_url(self):
        return reverse('regione_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.denominazione

    class Meta:
        ordering = ('denominazione',)
        verbose_name_plural = 'regioni'


class Settore(models.Model):
    denominazione = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from='denominazione', max_length=50, unique=True)
    codice = models.CharField(max_length=5, unique=True)

    def get_search_filter_querystring(self):
        return urllib.parse.urlencode({'selected_facets': 'settore:{}'.format(self.slug)}, safe=':')

    def get_absolute_url(self):
        return reverse('settore_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.denominazione

    class Meta:
        ordering = ('denominazione',)
        verbose_name_plural = 'settori'


class BaseQuota(models.Model):
    ente_partecipato_cronologia = models.ForeignKey(EntePartecipatoCronologia, on_delete=models.CASCADE)
    quota = models.DecimalField(max_digits=5, decimal_places=2)

    class Meta:
        ordering = ['ente_partecipato_cronologia', '-quota']
        abstract = True


class BaseQuotaEnte(BaseQuota):
    progressivo = models.SmallIntegerField()

    class Meta(BaseQuota.Meta):
        abstract = True


class EnteControllanteQuota(BaseQuotaEnte):
    ente_controllante = models.ForeignKey(EnteControllante, related_name='quote', on_delete=models.CASCADE)

    class Meta(BaseQuotaEnte.Meta):
        # ordering = ['ente_partecipato_cronologia', 'ente_controllante']
        unique_together = ('ente_partecipato_cronologia', 'ente_controllante')
        index_together = [
            ['ente_partecipato_cronologia', 'ente_controllante'],
        ]
        verbose_name_plural = 'quote enti controllanti'


class EnteControllatoQuota(BaseQuotaEnte):
    ente_controllato = models.ForeignKey(EnteControllato, related_name='quote', on_delete=models.CASCADE)

    class Meta(BaseQuotaEnte.Meta):
        # ordering = ['ente_partecipato_cronologia', 'ente_controllato']
        unique_together = ('ente_partecipato_cronologia', 'ente_controllato')
        index_together = [
            ['ente_partecipato_cronologia', 'ente_controllato'],
        ]
        verbose_name_plural = 'quote enti controllati'


class RegioneQuota(BaseQuota):
    regione = models.ForeignKey(Regione, related_name='quote', on_delete=models.CASCADE)

    class Meta(BaseQuota.Meta):
        # ordering = ['ente_partecipato_cronologia', 'regione']
        unique_together = ('ente_partecipato_cronologia', 'regione')
        index_together = [
            ['ente_partecipato_cronologia', 'regione'],
        ]
        verbose_name_plural = 'quote regioni'


class SettoreQuota(BaseQuota):
    settore = models.ForeignKey(Settore, related_name='quote', on_delete=models.CASCADE)

    class Meta(BaseQuota.Meta):
        # ordering = ['ente_partecipato_cronologia', 'settore']
        unique_together = ('ente_partecipato_cronologia', 'settore')
        index_together = [
            ['ente_partecipato_cronologia', 'settore'],
        ]
        verbose_name_plural = 'quote settori'


class Tooltip(models.Model):
    identificativo = models.SlugField(max_length=60, unique=True, db_index=True)
    testo = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.identificativo

    class Meta:
        ordering = ('identificativo',)
