from django import template
from django.contrib.humanize.templatetags.humanize import intword, intcomma

register = template.Library()


@register.filter
def halfsplit(value):
    value = list(value)
    n = -(-len(value) // 2)
    return [value[:n], value[n:]]


@register.filter
def bignumberformat(value):
    if value is None:
        return '-'
    return intcomma(intword(value)).replace('milioni', 'mln').replace('miliardi', 'mld')


@register.filter
def percentageformat(value):
    if value is None:
        return '-'
    return '{}%'.format(intcomma(round(value, 1)))
