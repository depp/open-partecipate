from django.utils.text import slugify
from haystack import indexes

from .models import EntePartecipato, EnteControllante


class EntePartecipatoIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True, stored=False)

    denominazione_slug = indexes.CharField(indexed=False, stored=False)

    in_attivita = indexes.FacetBooleanField(stored=False)

    regione = indexes.FacetMultiValueField(stored=False)
    settore = indexes.FacetMultiValueField(stored=False)
    categoria = indexes.FacetCharField(model_attr='categoria__slug', stored=False)
    sottotipo = indexes.FacetCharField(model_attr='sottotipo__slug', stored=False)

    spese_totali = indexes.FacetFloatField(stored=False)
    spese_investimenti_perc = indexes.FacetFloatField(stored=False)
    spese_personale_perc = indexes.FacetFloatField(stored=False)
    entrate_totali = indexes.FacetFloatField(stored=False)

    def get_model(self):
        return EntePartecipato

    def index_queryset(self, using=None):
        return self.get_model().objects.select_related('categoria', 'sottotipo').prefetch_related('cronologia__regionequota_set__regione', 'cronologia__settorequota_set__settore')

    def prepare_denominazione_slug(self, obj):
        return slugify(obj.denominazione)

    def prepare_in_attivita(self, obj):
        return obj.in_attivita

    def prepare_regione(self, obj):
        return list(obj.ultima_cronologia.regionequota_set.values_list('regione__slug', flat=True))

    def prepare_settore(self, obj):
        return list(obj.ultima_cronologia.settorequota_set.values_list('settore__slug', flat=True))

    def prepare_spese_totali(self, obj):
        return obj.ultima_cronologia.spese_totali

    def prepare_spese_investimenti_perc(self, obj):
        return obj.ultima_cronologia.spese_investimenti_perc

    def prepare_spese_personale_perc(self, obj):
        return obj.ultima_cronologia.spese_personale_perc

    def prepare_entrate_totali(self, obj):
        return obj.ultima_cronologia.entrate_totali


class EnteControllanteIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True, stored=False)

    denominazione_slug = indexes.CharField(indexed=False, stored=False)

    tipologia = indexes.FacetCharField(model_attr='tipologia__slug', stored=False)

    def get_model(self):
        return EnteControllante

    def index_queryset(self, using=None):
        return self.get_model().objects.select_related('tipologia')

    def prepare_denominazione_slug(self, obj):
        return slugify(obj.denominazione)
