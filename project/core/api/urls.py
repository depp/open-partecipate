from django.urls import include, path, re_path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *

router = routers.SimpleRouter()
router.register(r'partecipate', EntePartecipatoViewSet, basename='api-entepartecipato')
router.register(r'regioni', RegioneViewSet, basename='api-regione')
router.register(r'settori', SettoreViewSet, basename='api-settore')

urlpatterns = [
    re_path(r'^$', ApiRootView.as_view(), name='api-root'),

    path('dati-aggregati/', AggregatoHomeView.as_view(), name='api-aggregati-home'),
    path('dati-aggregati/regioni/<slug:slug>/', AggregatoRegioneDetailView.as_view(), name='api-aggregati-regione-detail'),
    path('dati-aggregati/settori/<slug:slug>/', AggregatoSettoreDetailView.as_view(), name='api-aggregati-settore-detail'),

    path('', include(router.urls)),

    # path('auth/', include('rest_framework.urls', namespace='rest_framework'))
]

urlpatterns = format_suffix_patterns(urlpatterns)
